#!/bin/sh
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="AppleTalk File Server"
NAME=netatalk
DAEMON=/usr/sbin/$NAME
VERBOSE=yes

dir_storage="/etc/storage/netatalk"
afpd_config="$dir_storage/afp.conf"

func_create_config()
{
	IP_ADDRESS=`ip address show br0 | grep -w inet | sed 's|.* \(.*\)/.*|\1|'`
	ROUTERNAME=`nvram get computer_name`
	DISKNAME=`ls /media | head -n1`
	ADMUSER=`nvram get http_username`
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Create config for $DESC" "$NAME"
	mkdir $dir_storage
	if [ ! -f "/etc/extmap.conf" ] ; then
		ln -sf /etc_ro/extmap.conf /etc/extmap.conf
	fi
	cat > "$afpd_config" <<EOF
;
; Netatalk 3.x configuration file
;
[Global]
	uam path = /usr/lib/netatalk
	uam list = uams_guest.so uams_dhx.so uams_dhx2.so
	admin auth user = "$ADMUSER"
	guest account = nobody
	save password = no
	sleep time = 1
	extmap file = /etc/extmap.conf
	mimic model = RackMac
;	tcprcvbuf = 262144
;	tcpsndbuf = 262144
;	recvfile = yes
;	splice size = 16384
;	log level = default:maxdebug
;	veto files = /.AppleDB/.AppleDouble/.AppleDesktop/:2eDS_Store/Network Trash Folder/Temporary Items/TheVolumeSettingsFolder	
[Storage]
	path = /media/$DISKNAME
	vol dbpath = /media/$DISKNAME/CNID
;	valid users = "$ADMUSER", nobody
	cnid scheme = dbd
	ea = auto
	file perm = 0664 directory perm = 0775
	time machine = yes
;	TM size in MB, ex. 1000 = 1GB
;	vol size limit = 500000
;	delete veto files = yes
;	recvfile = no
EOF
	/sbin/mtd_storage.sh save
}

func_start()
{
	if [ ! -d "$dir_storage" ] ; then
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Creating config for $DESC" "$NAME"
		func_create_config
	fi
	if [ -f "/var/lock/netatalk" ] ; then
		rm -f /var/lock/netatalk
	fi
	if [ ! -d "/var/netatalk/CNID" ] ; then
		mkdir -p /var/netatalk/CNID
	fi
	if [ -f $afpd_config ]; then
		# DHX login hack
		# ADMUSER=`nvram get http_username`
		# ADMPASS=`nvram get http_passwd`
		# MD5PASS=`openssl passwd -1 $ADMPASS`
		# MD5PASS=`cat /etc/shadow | grep $ADMUSER | cut -d":" -f2`
		# sed -i "s,$ADMUSER:x,$ADMUSER:$MD5PASS," /etc/passwd
		# start daemon
		$DAEMON -F $afpd_config
	else
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Configuration file $afpd_config for $DESC" "$NAME not found"
	fi
}

func_stop()
{
	killall -q $NAME
}

func_reload()
{
	kill -SIGHUP `pidof $NAME` 2>/dev/null
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Starting $DESC" "$NAME"
	func_start
	;;
  stop)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Stopping $DESC" "$NAME"
	func_stop
	;;
  force-reload)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Reloading $DESC" "$NAME configuration"
	func_reload
	;;
  restart)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Restarting $DESC" "$NAME configuration"
	func_stop
	sleep 1
	func_start
	;;
  *)
	echo "Usage: $0 {start|stop|restart|force-reload}"
	exit 1
	;;
esac

exit 0
