#!/bin/sh
PATH=/sbin:/usr/sbin:/bin:/usr/bin
DESC="AirPlay Synchronous Audio Service"
NAME=shairport-sync
DAEMON=/usr/bin/$NAME
VERBOSE=yes

# Exit if the package is not installed
[ -x "$DAEMON" ] || exit 0

dir_storage="/etc/storage/airplay"
airplay_config="$dir_storage/shairport-sync.conf"
alsa_config="$dir_storage/asound.conf"

func_check_audio()
{
	# is there default audio card present?
	if [ -c /dev/snd/pcmC0D0p ]; then
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Audio device found for $DESC" "$NAME"
	else
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "No audio device found for $DESC" "$NAME"
		# try to load audio modules
		insmod /lib/modules/`uname -r`/kernel/sound/soundcore.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd-timer.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd-page-alloc.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd-hwdep.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd-pcm.ko
		insmod /lib/modules/`uname -r`/kernel/sound/core/snd-rawmidi.ko
		insmod /lib/modules/`uname -r`/kernel/sound/usb/snd-usbmidi-lib.ko
		insmod /lib/modules/`uname -r`/kernel/sound/usb/snd-usb-audio.ko nrpacks=1
	fi
	# last try - goodbye !!!
#	[ -c /dev/snd/pcmC0D0p ] || exit 0
	# user defined alsa config
	if [ -f $alsa_config ] ; then
		ln -s $alsa_config /etc/asound.conf
	else
		cat > /etc/asound.conf <<EOC
# plug output to hardware
pcm.!default {
	type plug
	slave.pcm hw
}
EOC
	fi
}

func_create_config()
{
	mkdir $dir_storage
	cat > "$airplay_config" <<EOF
// General Settings
general =
{
name = "Air Player";
// password = "secret";
mdns_backend = "tinysvcmdns";
// port = 5000;
// udp_port_base = 6001;
// udp_port_range = 100;
// statistics = "no";
// drift = 88;
// resync_threshold = 2205;
// log_verbosity = 0;
// ignore_volume_control = "no";
// volume_range_db = 60 ;
};
// ALSA parameters
alsa =
{
output_device = "default";
// mixer_control_name = "PCM";
// mixer_device = "default";
// audio_backend_latency_offset = 0;
// audio_backend_buffer_desired_length = 6615;
};
// Static latencies for different sources
latencies =
{
// default = 88200;
// itunes = 99400;
// airplay = 88200;
// forkedDaapd = 99400;
};
EOF
		cat > "$alsa_config" <<EOS
# This is a simple configuration that will disable DMIX for ALSA, useful for configurations that uses USB speakers.
# Most USB sound card / speakers doesn't have a mixer, so ALSA will default to DMIX which has a poor quality.
# Disabling it will direct PCM to speaker without going through DMIX.
# Uncomment if you hear noises using ShairPort in WRT with USB speakers.

pcm.!default {
	type plug
	slave.pcm hw		# direct output to hardware
#	slave.pcm "dmixer"	# dmix plugin with 48000 Hz
}
pcm.dmixer  {
 	type dmix
 	ipc_key 1024
 	slave {
		pcm "hw:0,0"
		period_time 0
		period_size 1920
		buffer_size 19200
		rate 48000
	}
	bindings {
		0 0
		1 1
	}
}
ctl.dmixer {
	type hw
	card 0
}
EOS
	# save configuration
	/sbin/mtd_storage.sh save
}

#
# Function that starts the daemon/service
#
do_start()
{
	# check config presence, create if needed
	if [ ! -d "$dir_storage" ] ; then
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Creating config for $DESC" "$NAME"
		func_create_config
	fi
	# is there audio device?
	func_check_audio
	# You may also need to add route for 224.0.0.0/4 network to
	# your local network interface for tinysvcmdns advertisements, eg.
	if ! route | grep 224.0.0.0 | grep br0 > /dev/null; then
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Adding multicast LAN route for $DESC" "$NAME"
   		route add -net 224.0.0.0 netmask 224.0.0.0 br0
	fi
	# start service
	if [ -f $airplay_config ]; then
		$DAEMON -d -c $airplay_config
	else
		[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Configuration file $airplay_config for $DESC" "$NAME not found"
	fi
}

#
# Function that stops the daemon/service
#
do_stop()
{
	killall -q $NAME
#	$DAEMON -k
}

#
# Function that sends a SIGHUP to the daemon/service
#
do_reload() {
	kill -SIGHUP `pidof $NAME` 2>/dev/null
	return 0
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Starting $DESC" "$NAME"
	do_start
	;;
  stop)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Stopping $DESC" "$NAME"
	do_stop
	;;
  force-reload)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Reloading $DESC" "$NAME"
	do_reload
	;;
  restart)
	[ "$VERBOSE" != no ] && /usr/bin/logger -t $NAME "Restarting $DESC" "$NAME"
	do_stop
	do_start
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|restart|force-reload}" >&2
	exit 3
	;;
esac

:
