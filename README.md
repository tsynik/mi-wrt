# README #

### Welcome to the **MI-WRT** project ###

This project aims to improve Padavan's great fw on the software part, allowing power user to take full control over their hardware.
This project was created in hope to be useful, but comes without warranty or support. Installing it will probably void your warranty. 
Contributors of this project are not responsible for what happens next.

Currently added features:

* Full HFS+ filesystem support (with r/w access and fsck/mkfs tools)

* USB audio cards drivers

* AppleShare file server (AFP) ([netatalk](http://netatalk.sourceforge.net/) 3.1.11)

* AirPlay server ([shairport-sync](https://github.com/mikebrady/shairport-sync) 3.0.2)

* The Onion Router (TOR) (tor 0.2.9.10)

* Privoxy AD-proxy (privoxy 3.0.24)

...

### How do I get set up? ###

* Get the source to build firmware:
```
wget https://bitbucket.org/tsynik/mi-wrt/raw/e478fdd0d3c5d44d707b8ec6d2eae16e39c4fb0c/setup.sh
chmod 755 setup.sh
./setup.sh 
```
* Download pre-built system image here (https://bitbucket.org/tsynik/mi-wrt/downloads)
* Feed the device with the system image file (Follow instructions of updating your current system)
* Perform factory reset
* Open web browser on http://my.router to configure the services

### Contribution guidelines ###

* To be completed
