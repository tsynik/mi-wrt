#!/bin/bash

# !!! MUST exist in trunk/config/templates directory
# You can edit this later in configs/mi-wrt.config.sh
# before next build
RCONFIG="mi-mini"

# Available pre-configured setups,
# Set one of them in RCONFIG vafiable

# ASUS RT-N11:
# n11p_nano
# n11p_base

# ASUS RT=N14U:
# n14u_full
# n14u_base

# Xiaomi MiWiFi Mini:
# mi-mini

# Xiaomi MiWiFi Nano:
# mi-nano

# ASUS RT-N56U:
# n56u_aria
# n56u_dlna
# n56u_base
# n56u_trmd

# ASUS RT-N65:
# n65u_base
# n65u_full

# ASUS AC51U:
# ac51u_base
# ac51u_full

# ASUS AC54U:
# ac54u_base
# ac1200hp_base

# ASUS RT-N56 B.1:
# n56ub1_base
# n56ub1_full

RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[0;36m'
YELLOW='\033[1;33m'
NONE='\033[0m'
RED0='\033[0;31m'
GREEN0='\033[0;32m'
LOGO="$BLUE--------------------------------------------------------------------$NONE$GREEN
                      #\ /#  0   #   # ### #####
                      # ^ #  # = # # # # # ' # '
                      #   #  #    # #  #  #  #$NONE
$BLUE-------------------------------------------------------------------- $NONE"
REV=`git rev-parse --verify HEAD --short`
export REV
LOGO2="$GREEN                        +++ config script +++$NONE
$GREEN                          Codebase:$NONE $YELLOW$REV$NONE"
clear
echo -e "$LOGO"
sleep 0.2
echo -e "$LOGO2"
DIRS=`pwd`
export DIRS
DIRC="configs"

#
# configuration
#
function create_config {
echo -e "$BLUE Настройка конфигурации...$NONE"
sleep 0.5
if [ -f $DIRS/trunk/configs/templates/$RCONFIG.config ]; then
   # save config
   if [ ! -f $DIRS/$DIRC/mi-wrt.config.sh ]; then
       mkdir -p $DIRS/$DIRC
       cp -f $DIRS/trunk/configs/templates/$RCONFIG.config $DIRS/$DIRC/mi-wrt.config.sh
       sed -i "s|CONFIG_TOOLCHAIN_DIR=.*|CONFIG_TOOLCHAIN_DIR=$DIRS/toolchain-mipsel|" $DIRS/$DIRC/mi-wrt.config.sh
   fi
   # read parameters
   . ./$DIRC/mi-wrt.config.sh
else
   echo -e "$BLUE Не найдена конфигурация для сборки$NONE$RED $RCONFIG.config$NONE $BLUEв директории trunk/configs/templates$NONE"
   exit
fi
}

#
# functions
#
function user_wait {
   echo -e "$YELLOW Для продолжения нажмите любую клавишу... $NONE"
   read -n1 -s ; read -s -t 0.1
}

function user_quit {
echo -e "$RED"
   while true; do
      read -p " Вы уверены? (Y/N) " yn
      case $yn in
         [Yy]* ) echo -e "$NONE"; exit;;
         [Nn]* ) echo -e "$NONE"; return;;
             * ) echo -e " Пожалуйста, введите yes или no.";;
      esac
   done
}

function do-toolchain {
   echo -e "$BLUE Сборка toolchain... $NONE"
   cd $DIRS/toolchain-mipsel
   if [[ "$CONFIG_LINUXDIR" != *"3.0"* ]]; then
       ./build_toolchain 2>&1 | tee -ia $DIRS/logs/build_toolchain.log
   else
       ./build_toolchain_3.0.x 2>&1 | tee -ia $DIRS/logs/build_toolchain.log
   fi

   if [[ "$CONFIG_LINUXDIR" == *"3.0"* && -d $DIRS/toolchain-mipsel/toolchain-3.0.x ]]; then
       echo -e "$BLUE Toolchain:$NONE$GREEN    OK $NONE"
       TCH="$BLUE Toolchain:$NONE$GREEN    OK $NONE"
       TCP=1
       sleep 1
   elif [[ "$CONFIG_LINUXDIR" != *"3.0"* && -d $DIRS/toolchain-mipsel/toolchain-3.4.x ]]; then
       echo -e "$BLUE Toolchain:$NONE$GREEN    OK $NONE"
       TCH="$BLUE Toolchain:$NONE$GREEN    OK $NONE"
       TCP=1
       sleep 1
   else
       clear
       echo -e "$GREEN/-----------------------------------------------------------/$NONE";
       grep -iE "(ошибка)|(останов)|(\smissing\s)|(failed$)|(error ([0-9])+$)|(not found$)|(aborted\!)|(stop\.$)" $DIRS/logs/build_toolchain.log | awk '!a[$0]++'
       echo -e "$GREEN/-----------------------------------------------------------/$NONE";
       echo -e "$BLUE Toolchain:$NONE$RED ERROR$NONE"
       echo -e "$BLUE В процессе сборки toolchain произошла ошибка! $NONE"
       user_wait
       TCP=0
   fi
   cd $DIRS
}

function do-build {
   # проверяем toolchain
   if [ $TCP -eq 0 ]; then
       cd $DIRS/toolchain-mipsel
       do-toolchain
       if [ $TCP -eq 0 ]; then
                return
       fi
   fi
   # перезаписываем конфигурацию
   if [ -f $DIRS/$DIRC/mi-wrt.config.sh ]; then
       echo -e "$BLUE Используется конфигурация для сборки$NONE$GREEN mi-wrt.config.sh$NONE$BLUE в директории $DIRC.$NONE"
       cp -f $DIRS/$DIRC/mi-wrt.config.sh $DIRS/trunk/.config
   else
       echo -e "$BLUE Не найдена конфигурация для сборки$NONE$RED mi-wrt.config.sh$NONE$BLUE в директории $DIRC.$NONE"
       return
   fi
   sed -i "s|CONFIG_TOOLCHAIN_DIR=.*|CONFIG_TOOLCHAIN_DIR=$DIRS/toolchain-mipsel|" $DIRS/trunk/.config

   # собираем прошивку
   cd $DIRS/trunk
   # echo -e "$BLUE Очистка исходного кода... $NONE"
   # ./clear_tree
   echo -e "$BLUE Сборка firmware... $NONE"
   mv -f $DIRS/logs/build_firmware.log $DIRS/logs/build_firmware.old >/dev/null 2>&1
   # rm $DIRS/logs/build_firmware.log >/dev/null 2>&1
   ./build_firmware 2>&1 | tee -ia $DIRS/logs/build_firmware.log
   if [ -d $DIRS/trunk/images* ]; then
       cd $DIRS/trunk/images
       # Ищем прошивку
       FF=`find . -type f -iname "$CONFIG_FIRMWARE_PRODUCT_ID*.trx"`
       if [[ $FF == *$CONFIG_FIRMWARE_PRODUCT_ID*.trx ]]; then
           FFM=`echo "$FF" | sed 's/^\.\///'`
           TRX=`echo "$FFM" | sed "s|.trx|_$REV|"`
           FFFM=$DIRS/trunk/images/$FFM
           # Проверяем размер прошивки
           firmware_size=$(stat -c %s $FFFM)
           max_allowed_size=$(grep -oE "can't bigger than ([0-9])+ !!!" $DIRS/logs/build_firmware.log | grep -oE "([0-9])+")
           if [[ ! -z $max_allowed_size ]] && [ $firmware_size -gt $max_allowed_size ]; then
               echo -e "$RED Размер $TRX.trx = $firmware_size B и превышает допустимый $max_allowed_size B $NONE"
               rm $FFFM
               echo -e "$RED Прошивка уничтожена в целях безопасности!\n" \
               "Измените конфигурацию и пересоберите ее.$NONE"
               user_wait
           else
               FIRM="$BLUE Firmware:$NONE$YELLOW $TRX $NONE"
               echo -e "$FIRM"
               if [[ ! -z $max_allowed_size ]]; then
                   echo -e "$GREEN Размер $TRX.trx = $firmware_size байтов и соответствует допустимому $max_allowed_size байтов $NONE"
               fi
               #Сохраняем прошивку в архив
               if [ ! -d $DIRS/files ]; then
                   mkdir $DIRS/files
               fi
               trx_date=$(date +%Y-%m-%d)
               trx_time=$(date +%H:%M:%S)
               mkdir -p $DIRS/files/$trx_date
               cp $FFFM $DIRS/files/$trx_date/${TRX}_${trx_time}.trx
               cp -f $DIRS/trunk/.config $DIRS/files/$trx_date/${TRX}_${trx_time}-build.config
               cp -f $DIRS/trunk/configs/boards/$CONFIG_FIRMWARE_PRODUCT_ID/board.h $DIRS/files/$trx_date/${TRX}_${trx_time}-board.h
               echo -e "$BLUE Файлы прошивки помещены в директорию $NONE$YELLOW $DIRS/files/$trx_date $NONE"
               sleep 1
               cd $DIRS
               user_wait
               return
           fi
       fi
   fi
   user_wait
}

function do-clean {
   cd $DIRS/trunk
   echo -e "$BLUE Очистка исходного кода... $NONE"
   ./clear_tree
   echo -e "$RED"
   while true; do
        read -p " Отменить так же все локальные изменения (git reset --hard)? (Y/N) " yn
        case $yn in
              [Yy]* ) echo -e "$BLUE Полный сброс исходного кода... $NONE"; cd $DIRS; git reset --hard; sleep 3; exec ./start.sh;;
              [Nn]* ) echo -e "$NONE"; break;;
               * ) echo -e " Пожалуйста, введите yes или no.";;
        esac
   done
   echo -e "$YELLOW"
   while true; do
        read -p " Обновить исходный код до актуального (git pull)? (Y/N) " yn
        case $yn in
              [Yy]* ) echo -e "$BLUE Обновление исходного кода... $NONE"; cd $DIRS; git pull; sleep 3; exec ./start.sh;;
              [Nn]* ) echo -e "$NONE"; break;;
               * ) echo -e " Пожалуйста, введите yes или no.";;
        esac
   done
   user_wait
}

function do-config {
   echo -e "$RED Измените открывшийся файл, решетка перед\n" \
   "параметром его выключает.\n" \
   "Вы так же можете добавлять новые параметры.\n" \
   "После внесения изменений сохраните их:\n" \
   "Ctrl-X, Y, Enter\n$NONE"
   user_wait
   if [ ! -f $DIRS/$DIRC/mi-wrt.config.sh ]; then
        # cd $DIRS/trunk/configs/templates
        # sed -i "s|CONFIG_TOOLCHAIN_DIR=.*|CONFIG_TOOLCHAIN_DIR=$DIRS/toolchain-mipsel|" $DIRS/trunk/configs/templates/$RCONFIG.config
        cp -f $DIRS/trunk/configs/templates/$RCONFIG.config $DIRS/$DIRC/mi-wrt.config.sh
   fi
   sed -i "s|CONFIG_TOOLCHAIN_DIR=.*|CONFIG_TOOLCHAIN_DIR=$DIRS/toolchain-mipsel|" $DIRS/$DIRC/mi-wrt.config.sh
   nano $DIRS/$DIRC/mi-wrt.config.sh
   echo -e "$BLUE Config,$NONE$GREEN OK $NONE"
   cd $DIRS
}

function switch-router {
   echo -e "$BLUE Доступные конфигурации:$NONE"
   i=0
   for config in `ls $DIRS/trunk/configs/templates/*.config`; do
       i=$(($i + 1))
       CONFIGN["$i"]=$config
       template=`basename $config | sed "s|.config||"`
       echo -e "$GREEN $i:$NONE$YELLOW $template $NONE"
   done
   if [[ ! -z $template ]]
   then
      while true; do
         read -p " Ввведите номер конфигурации, которую вы хотите использовать: " yn
         if [[ -z ${CONFIGN[$yn]} ]]
         then
            echo -e "$RED Пожалуйста, введите существующий номер $NONE"
            sleep 1
         else
            RCONFIG=`basename "${CONFIGN[$yn]}" | sed "s|.config||"`
            echo -e "$RED Выбрана конфигурация ${RCONFIG} $NONE"
            break
         fi
      done
   fi
   user_wait
   if [ -f $DIRS/$DIRC/mi-wrt.config.sh ]; then
      mv -f $DIRS/$DIRC/mi-wrt.config.sh $DIRS/$DIRC/mi-wrt.config.old
   fi
   create_config
}

#
# main routine
#
create_config
while :
do
   sleep 0.2
   clear
   echo -e "$LOGO"
   sleep 0.2
   echo -e "$LOGO2"
   sleep 0.2

   if [[ "$CONFIG_LINUXDIR" == *"3.0"* && -d $DIRS/toolchain-mipsel/toolchain-3.0.x ]]
   then
      echo -e "$BLUE Toolchain:$NONE$GREEN    OK $NONE"
      TCH="$BLUE Toolchain:$NONE$GREEN    OK $NONE"
      TCP=1
      sleep 0.1
   elif [[ "$CONFIG_LINUXDIR" != *"3.0"* && -d $DIRS/toolchain-mipsel/toolchain-3.4.x ]]
   then
      echo -e "$BLUE Toolchain:$NONE$GREEN    OK $NONE"
      TCH="$BLUE Toolchain:$NONE$GREEN    OK $NONE"
      TCP=1
      sleep 0.1
   else
      echo -e "$BLUE Toolchain:$NONE$YELLOW    NONE $NONE"
      TCH="$BLUE Toolchain:$NONE$YELLOW    NONE $NONE"
      sleep 0.1
      TCP=0
   fi

   echo -e "$BLUE Router ID:$NONE$GREEN    $CONFIG_FIRMWARE_PRODUCT_ID$NONE"

cat<<EOF1
 Пожалуйста, введите номер желаемого пункта:
 Выбрать роутер               (0)
 Изменить config сборки       (1)
 Собрать toolchain            (2)
 Очистить/обновить код        (3)
 Собрать Firmware             (4)
 Выход                        (Q)
EOF1
   read -n1 -s
   case "$REPLY" in
    "0")  switch-router ;;
    "1")  do-config ;;
    "2")  do-toolchain ;;
    "3")  do-clean ;;
    "4")  do-build ;;
 [Qq]*)  user_quit ;;
      *)  echo -e "$RED Нет такой команды! $NONE"
          sleep 0.3 ;;
   esac
done
