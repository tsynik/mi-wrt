#!/bin/bash
RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[0;36m'
YELLOW='\033[1;33m'
NONE='\033[0m'
RED0='\033[0;31m'
GREEN0='\033[0;32m'
LOGO="$BLUE--------------------------------------------------------------------$NONE$YELLOW
                      #\ /#  0   #   # ### #####
                      # ^ #  # = # # # # # ' # '
                      #   #  #    # #  #  #  #$NONE
$BLUE-------------------------------------------------------------------- $NONE"
LOGO2="$YELLOW                        +++ setup script +++$NONE"  
clear
echo -e "$LOGO"
sleep 0.2
echo -e "$LOGO2"
if [[ $(basename $PWD) == "mi-wrt" ]]; then
   DIRS=`pwd`
else
   DIRS=`pwd`/mi-wrt
   mkdir $DIRS 1>/dev/null 2>&1
fi
export DIRS

   echo -e "$BLUE Проверка соединения с сетью...$NONE"
   case "$(curl -s --max-time 2 -I http://bitbucket.org | sed 's/^[^ ]*  *\([0-9]\).*/\1/; 1q')" in
     [23]) internet_connection=ok ;;
     5) internet_connection=error ;;
     *) internet_connection=ok ; echo "Соединение с интернетом медленное";;
   esac

   if [ "$internet_connection" == "error" ]
   then
      echo "$RED Cервер BitBucket не отвечает! $NONE"
      while true; do
         read -p " Все равно продолжить? " yn
         case $yn in
            [Yy]* ) echo -e "$NONE"; break;;
            [Nn]* ) echo -e "$NONE"; exit;;
                * ) echo -e " Пожалуйста введите yes или no.";;
         esac
      done
   fi

   echo -e "$BLUE Подготовка окружения linux...$NONE"
   while true; do
      #проверяем зависимости и выходим если все установлено
      dpkg -s build-essential gawk texinfo pkg-config gettext automake libtool bison flex zlib1g-dev libgmp3-dev libmpfr-dev libmpc-dev git zip sshpass mc curl python expect bc telnet openssh-client autopoint >/dev/null 2>&1 && break 
      echo -e "$YELLOW Устанавливаем ПО, требуется ввести пароль от $NONE"
      echo -e "$YELLOW вашей учетной записи в Linux. $NONE"
      sudo apt-get update
      sudo apt-get -y --force-yes install build-essential gawk texinfo pkg-config gettext automake libtool bison flex zlib1g-dev libgmp3-dev libmpfr-dev libmpc-dev git zip sshpass mc curl python expect bc telnet openssh-client autopoint
   done
   echo -e "$BLUE Загрузка исходного кода...$NONE"
   cd $DIRS
   if [ -d .git ]; then
   echo -e "$BLUE Обновление исходного кода...$NONE"
#       git pull https://bitbucket.org/tsynik/mi-wrt.git
   else
       cd ..
       git clone https://bitbucket.org/tsynik/mi-wrt.git
       cd $DIRS
       echo -e "$BLUE Cоздание директорий...$NONE"
       # Проверяем наличие директорий
       rm -R $DIRS/scripts &>/dev/null
       mkdir $DIRS/scripts
       rm -R $DIRS/configs &>/dev/null
       mkdir $DIRS/configs
       rm -R $DIRS/files &>/dev/null
       mkdir $DIRS/files
       rm -R $DIRS/logs &>/dev/null
       mkdir $DIRS/logs
   fi
   if [ -f start.sh ]; then
      echo -e "$BLUE Скрипт запуска:$NONE$GREEN      OK $NONE"
      sleep 0.1
      exec ./start.sh
   else
      echo -e "$BLUE Скрипт запуска:$NONE$RED      NO $NONE"
      sleep 0.1
   fi

